import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import * as camera from "nativescript-camera";
import * as imageSourceModule from "tns-core-modules/image-source";
import * as SocialShare from "nativescript-social-share";
//import { isAndroid, isIOS, device, screen as sc0 } from "tns-core-modules/platform";

@Component({
    selector: "Home",
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {

    constructor() {

    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onButtonTap(): void {
        camera.requestPermissions().then(
            function success() {
                const options = { width: 300, height: 300, keepAspectRatio: false, saveToGallery: true };
                camera.takePicture(options).then((imageAsset) => {
                    console.log("Tamaño: " + imageAsset.options.width + "x" + imageAsset.options.height);
                    console.log("KeepAspectRatio: " + imageAsset.options.keepAspectRatio);
                    console.log("Foto guardada!");
                    /*imageSourceModule.fromAsset(imageAsset).then((imageSource) => {
                        SocialShare.shareImage(imageSource, "Asunto: compartido desde el curso!");
                    }).catch((err) => {
                        console.log("Error -> " + err.message);
                    });*/
                }).catch((err) => {
                    console.log("Error -> " + err.message);
                })
            },
            function failure() {
                console.log("Permiso de cámara no aceptador por el usuario.");
            }
        )
    }

    /*
    onDatosPlataforma(): void {
        console.log("modelo", device.model);         
        console.log("tipo dispositivo", device.deviceType);
        console.log("Sistema operativo", device.os);         
        console.log("versión sist operativo", device.osVersion);         
        console.log("Versión sdk", device.sdkVersion);         
        console.log("lenguaje", device.language);         
        console.log("fabricante", device.manufacturer);         
        console.log("código único de dispositivo", device.uuid);         
        console.log("altura en pixels normalizados", sc0.mainScreen.heightDIPs); // DIP (Device Independent Pixel), también conocido como densidad de píxeles independientes. Un píxel virtual que aparece aproximadamente del mismo tamaño en una variedad de densidades de pantalla.
        console.log("altura pixels", sc0.mainScreen.heightPixels);         
        console.log("escala pantalla", sc0.mainScreen.scale);
        console.log("ancho pixels normalizados", sc0.mainScreen.widthDIPs);
        console.log("ancho pixels", sc0.mainScreen.widthPixels);    
    }
    */



}



