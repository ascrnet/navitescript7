import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, Color, View } from "@nativescript/core";
import { NoticiasService } from "../domain/noticias.services";
import * as Toast from "nativescript-toasts";
import { AppState } from "../app.module";
import { Store } from "@ngrx/store";
import { Noticia, NuevaNoticiaAction } from "../domain/noticias-state.model";
import * as SocialShare from "nativescript-social-share";

@Component({
    selector: "Search",
    templateUrl: "./search.component.html"
})
export class SearchComponent implements OnInit {

    resultados: Array<string>;
    @ViewChild("layout", { static: false }) layout: ElementRef;

    constructor(private noticias: NoticiasService, private store: Store<AppState>) {
     
    }

    ngOnInit(): void {
        //this.noticias.agregar("Hola !!!");
        //this.noticias.agregar("Hola 2 !!!");
        //this.noticias.agregar("Hola 3 !!!");
        this.store.select((state) => state.noticias.sugerida)
        .subscribe((data) => {
            const f = data;
            if (f != null) {
                Toast.show({ text:"Sugerimos leer: " + f.titulo, duration: Toast.DURATION.SHORT});
            }
        })
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args): void {
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));
    }
    
    onLongPress(s): void {
        console.log(s);
        SocialShare.shareText(s, "Asunto: compartido desde el curos!");
    }

    buscarAhora(s: string) {
        console.dir("buscarAhora" + s);
        this.noticias.buscar(s).then((r: any) => {
            console.log("resultados buscarAhora: " + JSON.stringify(r));
            this.resultados = r;
        }, (e) => {
            console.log("error buscarAhora " + e);
            Toast.show({text: "Error en la búsqueda", duration: Toast.DURATION.SHORT});
        });
    }

    /*
    buscarAhora(s: string) {
        this.resultados = this.noticias.buscar().filter((x) => x.indexOf(s) >= 0 );
        const layout = <View>this.layout.nativeElement;
        layout.animate({
            backgroundColor: new Color("blue"),
            duration: 3000,
            delay: 1500
        }).then(() => layout.animate({
            backgroundColor: new Color("white"),
            duration: 3000,
            delay: 1500           
        }));
    } */

}
