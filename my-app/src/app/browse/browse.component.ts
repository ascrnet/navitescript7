import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import { registerElement } from "nativescript-angular/element-registry";

registerElement("MapView", () => require("nativescript-google-maps-sdk").MapView);
var gmaps = require("nativescript-google-maps-sdk");

@Component({
    selector: "Browse",
    templateUrl: "./browse.component.html"
})
export class BrowseComponent implements OnInit {

    @ViewChild("MapView", { static: false }) mapView: ElementRef;

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onMapReady(event): void {
        console.log("Map Ready");

        var mapView = event.object;
        var marker = new gmaps.Marker();

        marker.position = gmaps.Position.positionFromLatLng(-34.6037, -58.3817);
        marker.title = "Buenos Aires";
        marker.snippet = "Argentina";
        marker.userData = { index : 1 };
        mapView.addMarker(marker);
    }
}
