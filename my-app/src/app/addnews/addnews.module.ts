import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "@nativescript/angular";

import { AddNewsRoutingModule } from "./addnews-routing.module";
import { AddNewsComponent } from "./addnews.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        AddNewsRoutingModule
    ],
    declarations: [
        AddNewsComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AddNewsModule { }
