import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, TextField } from "@nativescript/core";
import { NoticiasService } from "../domain/noticias.services";

@Component({
    selector: "Add",
    templateUrl: "./addnews.component.html"
})
export class AddNewsComponent implements OnInit {

    constructor(public noticias: NoticiasService) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {

    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

   /* onReturnPress(args) {
        let textField = <TextField>args.object;
        this.noticias.agregar( textField.text);
    }*/

    onButtonTap(): void {
        alert('thanks for the news');
    }

}

