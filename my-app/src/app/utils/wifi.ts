/*

agrega lo siguiente en app/App_Resources/Android/AndroidManifest.xml: <uses-permission android: name = "android.permission.ACCESS_NETWORK_STATE" />


import { connectionType, getConnectionType, startMonitoring, stopMonitoring } from "tns-core-modules/connectivity"; 

monitoreando: boolean = false; // una variable para saber si estás monitoreando o no. 

onMonitoreoDatos(): void {
    const myConnectionType = getConnectionType();  
    switch (myConnectionType) {             
        case connectionType.none:
            console.log("Sin Conexion");
            break;             
        case connectionType.wifi: 
            console.log("WiFi"); 
            break; 
        case connectionType.mobile:
            console.log("Mobile"); 
            break;
        case connectionType.ethernet:
            console.log("Ethernet"); // es decir, cableada 
            break;             
        case connectionType.bluetooth:
            console.log("Bluetooth");
            break;             
        default:                 
            break;
        this.monitoreando = !this.monitoreando;
        if (this.monitoreando) {             
            startMonitoring((newConnectionType) => { 
                switch (newConnectionType) {   
                    case connectionType.none:                         
                        console.log("Cambió a sin conexión.");
                        break;                    
                    case connectionType.wifi:
                        console.log("Cambió a  WiFi."); 
                        break; 
                    case connectionType.mobile:
                        console.log("Cambió a  mobile.");
                        break; 
                    case connectionType.ethernet:
                        console.log("Cambió a  ethernet.");
                        break;
                    case connectionType.bluetooth:
                        console.log("Cambió a bluetooth.");
                        break;                     
                    default:
                        break;
                } 
            });
        } else { 
            stopMonitoring();         
        }     
    }


*/